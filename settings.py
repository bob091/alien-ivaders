""" Klasa ustawień dla gry """
class Settings() :
    def __init__(self):
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (0, 0, 0)

        #ustawienia dotyczące statku
        self.ship_limit = 3

        #ustawienia dotyczące pocisków
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (255, 0, 0)
        self.bullet_allowed = 5 #maksymalna liczba pocisków na ekranie

        #Ustawienia dotyczące obcego
        self.fleet_drop_speed = 10

        #zmiana szybkości gry
        self.speedup_scale = 1.1

        #punktacja
        self.alien_score = 50

        #zmiana punktów za zestrzelenie obcego
        self.score_scale = 1.5

        #najlepszy wynik
        self.heigh_score = 0

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """Inicjalizacja ustawień gry które zmieniają się w czasie gry"""
        self.ship_speed = 1.5
        self.bullet_speed = 1.0
        self.alien_speed = 0.5

        # wartość fleet_dierction = 1 oznacza prawo -1 lewo
        self.fleet_dierction = 1

    def increase_speed(self):
        self.ship_speed *= self.speedup_scale
        self.bullet_speed *= self.speedup_scale
        self.alien_speed *= self.speedup_scale

        self.alien_score *= self.score_scale


