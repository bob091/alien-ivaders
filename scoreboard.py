import pygame.font
from pygame.sprite import Group

from ship import Ship

class Scoreboard():
    """Klasa przeznaczona do obsługi informacji o punktacji"""

    def __init__(self, ai_game):
        """Inicjalizacja atrybutów dotyczących punktacji"""
        self.ai_game = ai_game
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()
        self.settings = ai_game.settings
        self.stats = ai_game.stats

        #Ustawinei czciomki i koloru informacji o punktacji
        self.text_color = (0, 255, 255)
        self.font = pygame.font.SysFont(None, 48)

        #Przygotowanie początkowego obrazu z punktacją
        self.prep_score()
        self.prep_hight_score()
        self.prep_level()
        self.prep_ships()

    def prep_score(self):
        """Przekrztałcenie punktacji na wygenerowany obraz """
        rounded_score = round(self.stats.score, -1)
        score_str = "{:,}".format(rounded_score)
        self.score_image = self.font.render(score_str, True, self.text_color, self.settings.bg_color)

        #Wyświetlanie punktacji w prawym górnnym rogu
        self.score_rect = self.score_image.get_rect()
        self.score_rect.right = self.screen_rect.right - 20
        self.score_rect.top = 20

    def prep_hight_score(self):
        """Konwersja najlepszego wyniku w grze na wygenerowany obraz"""
        hight_score = round(self.stats.hight_score, -1)
        hight_score_str = "{:,}".format(hight_score)
        self.hight_score_image = self.font.render(hight_score_str, True, self.text_color, self.settings.bg_color)

        #Wyświetlenie najlepszego wyniku w grze na śwrodku ekranu
        self.hight_score_rect = self.hight_score_image.get_rect()
        self.hight_score_rect.centerx = self.screen_rect.centerx
        self.hight_score_rect.top = self.score_rect.top

    def prep_level(self):
        """Konwersja numeru poziomu na obraz wyświetlany na ekranie"""
        level_str = str(self.stats.level)
        self.level_image = self.font.render(level_str, True, self.text_color, self.settings.bg_color)

        #Wyświetlanie numeru poziomu pod punkacją
        self.level_rect = self.level_image.get_rect()
        self.level_rect.right = self.score_rect.right
        self.level_rect.top = self.score_rect.bottom + 10

    def check_hight_score(self):
        """Sprawdzanie czy mamy nowy najwyrzszy wynik"""
        if self.stats.score > self.stats.hight_score:
            self.stats.hight_score = self.stats.score
            self.prep_hight_score()

    def prep_ships(self):
        """ Wyświtla liczbę staków jakie zostaly graczowi """
        self.ships = Group()
        for ship_numebr in range(self.stats.ship_left):
            ship = Ship(self.ai_game)
            # miniaturki do wyświtlenia w lewym górnym rogu
            ship.rect.x = 10 + ship_numebr * ship.rect.width
            ship.rect.y = 10
            self.ships.add(ship)

    def show_score(self):
        """Wyświetlanie punkatcji na ekranie"""
        self.screen.blit(self.score_image, self.score_rect)
        self.screen.blit(self.hight_score_image, self.hight_score_rect)
        self.screen.blit(self.level_image, self.level_rect)
        self.ships.draw(self.screen)
