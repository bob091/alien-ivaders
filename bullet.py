import  pygame
from pygame.sprite import Sprite

class Bullet(Sprite):
    """ kalsa przenaczona do zarządzania pociskiem wystrzlonym przez statek """
    def __init__(self, ai_game):
        """Utwozrenie obiektu przycisku w aktualnym położeniu statku"""
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.color = self.settings.bullet_color

        #Utwozrenie prosyokąta pocisku wpuncie 0,0
        #a nstepnie zdefiniowanie go w odpowiednim miejscu
        self.rect = pygame.Rect(0, 0, self.settings.bullet_width, self.settings.bullet_height)
        self.rect.midtop = ai_game.ship.rect.midtop

        # położenie jest zdefiniowane za pomkcą liczby zmienno przecinkowej
        self.y = float(self.rect.y)

    def update(self):
        """poruszanie pocisku na ekrania """
        #uaktualnianie położenia pocisku
        self.y -=self.settings.bullet_speed
        #uaktualniaie położnie prostokata pocisku
        self.rect.y = self.y

    def draw_bullet(self):
        """ wyświetlanie pocisku na ekranie """
        pygame.draw.rect(self.screen, self.color, self.rect)