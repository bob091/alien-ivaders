import pygame
from pygame.sprite import Sprite

class Ship(Sprite):
    """Klasa odpowiedzialna za zarządzanie statkiem"""
    def __init__(self, ai_game):
        """Inicjalizacja statku i jego początkowe położenie"""
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.screen_rect = ai_game.screen.get_rect()

        #wczytanie obrazu statku
        self.image = pygame.image.load('images/ship.bmp')
        self.rect = self.image.get_rect()

        #Każdy nowy statek pojawia sie na dole ekrany
        self.rect.midbottom = self.screen_rect.midbottom

        #położenie pozime statku wpostaci liczby zmienno przcinkowej
        self.x = float(self.rect.x)

        #opcja wskazująca na poruszanie się statku
        self.moving_right = False
        self.moving_left = False

    def update(self):
        """Uaktualnienie położenia statku na podstawie opcji w skazującej jego ruch"""
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.ship_speed
        if self.moving_left and self.rect.left > 0:
            self.x -= self.settings.ship_speed

        #Uaktualnienie oboektu rect na ppdstawie self.x
        self.rect.x = self.x

    def center_ship(self):
        """Metoda cenytrująca statek na ekranie"""
        self.rect.midbottom = self.screen_rect.midbottom
        self.x - float(self.rect.x)

    def blitme(self):
        """ wyświetlanie statku na jego aktualnym położeniu"""
        self.screen.blit(self.image, self.rect)
