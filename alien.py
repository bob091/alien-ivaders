import pygame
from pygame.sprite import Sprite

class Alien(Sprite):
    """Klasa opisująca obcego """
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings

        #wczytanie obrazka obcego
        self.image = pygame.image.load('images/alien.bmp')
        self.rect = self.image.get_rect()

        #umieszczenie obcego w lewym gornym rogu
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        # przechowywyanie dokąłdnego połżenia pionowego obcego
        self.x = float(self.rect.x)

    def check_edges(self):
        """Zwraca TRUE jeśli dotrze do krawędzi ekranu"""
        screen_rect = self.screen.get_rect()
        if self.rect.right >= screen_rect.right or self.rect.left <= 0: #Sprawdzenie dotarcia do krawędzi
            return True

    def update(self):
        """Przesunięcie obcego w prawo"""
        self.x += (self.settings.alien_speed * self.settings.fleet_dierction)
        self.rect.x = self.x