class GameStats():
    """Dane statystyczne w grze"""

    def __init__(self, ai_game):
        """Iniecjalizacja danych statystycznych w grze"""
        self.settings = ai_game.settings
        self.reset_stats()

        #Uruchomienie gry w trycbye aktywnym
        self.game_active = False
        self.hight_score = 0

    def reset_stats(self):
        """Inicjalizacja danych statystycznych które mogo się znaleźć w grze"""
        self.ship_left = self.settings.ship_limit
        self.score = 0
        self.level = 0