import pygame.font

class Button():
    """Klasa odpowiedzkialna za obsługę przycisków"""
    def __init__(self, ai_game, msg):  # msg- reperzentuje text wyświtlany na przycisku
        """Inicjalizacja atrybutów przycisku """
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()

        # Zdefiniowani wymiarów i włściwiści przycisku
        self.width = 200
        self.height = 50
        self.buttom_color = (255, 0, 0)
        self.text_color = (255, 255, 255)
        self.font = pygame.font.SysFont(None, 48)  # None-czcionka domyślna, 48 rozmiar czcionki

        # utworzenie prostokąta przycisku i wyświetlanie go
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.center = self.screen_rect.center

        # komunikta wyświetlany przez przycisk
        self._prep_msg(msg)

    def _prep_msg(self, msg):
        """ Umieszczenie napisu w wygenerowanym obrazie na przycisku i wysierodkowanie go """
        self.msg_image = self.font.render(msg, True, self.text_color, self.buttom_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def draw_button(self):
        """ wyświetlanei pystego przycisku a następnie komunikatu na nim """
        self.screen.fill(self.buttom_color, self.rect)  # wyśeitlamy prostokąt reperzentujący przycisk
        self.screen.blit(self.msg_image, self.msg_image_rect)  # wyświetlanei obrazu tekstu na przycisku
