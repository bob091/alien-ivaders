#Alien Invaders mine file
import sys
import time

from settings import Settings
from game_stats import GameStats
from scoreboard import Scoreboard
from button import Button
from ship import Ship
from alien import Alien
from bullet import Bullet

import pygame

class AlienInvaders() :
    """ Ogulna klasa przeznaczona zarządzaniem zasobami gry
            i sposobem działania gry
    """
    def __init__(self):
        """ Inicjalizacja gry i utworzenie jej zasobów"""
        pygame.init()

        self.settings = Settings()
        self.screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN) #Otwieranie gry w pełno ekranowym
        self.settings.screen_width = self.screen.get_rect().width
        self.settings.screen_height = self.screen.get_rect().height
        pygame.display.set_caption("Atak Obcych")

        #utworzenie egzemplarza klasy danych statystycznych
        #Utworzenie egzemplarza klasy scoreboard
        self.stats = GameStats(self)
        self.sb = Scoreboard(self)

        self.ship = Ship(self)
        self.bullets = pygame.sprite.Group()
        self.aliens = pygame.sprite.Group()

        self._create_fleet()

        #Utworzenie przycisku "GRA"
        self.play_button = Button(self, "GRA")

    def run_game(self):
        """Rozpoczęcie pętli głównej gry"""
        while True:
            self._check_events()

            if self.stats.game_active : # Elemnty są tworzone tylko gdy gra jest aktywna
                self.ship.update()
                self._update_bullet()
                self._update_alien()

            self._update_screen()

    def _check_events(self):
        """ Motoda pomsnicza(rozpoczyna się od znaku '_')
            Metoda odpowiedzialna za reakcje na zdarzenia muszy i klawiatury """
        #oczekiwanie na naciśnięcie klawisza lub myszy
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:  # reakcja na naciśniecie przycisk myszy
                mouse_pos = pygame.mouse.get_pos()  # pobranie pozycji myszy
                self._check_play_button(mouse_pos)

    def _check_keydown_events(self, event):
        """Reakcja na naciśnięcie klawisza (True oznnacza naciśnięcie klaiwsza)"""
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_ESCAPE:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()

    def _check_keyup_events(self, event):
        """Reakcja na  puszczenie klawisza"""
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = False

    def _check_play_button(self, mouse_pos):
        """Rozpoczęcie gry po naciśnięciu przycisku gra"""
        button_cliked = self.play_button.rect.collidepoint(mouse_pos)# na podstawie wykrycia kolizji określamy czy przycisk został naciśnięty
        if button_cliked and not self.stats.game_active: #Przyciks aktywny tylko gdy game_active=False
            #zerowanie damych statystycznych w grze
            self.settings.initialize_dynamic_settings()  # wyzerowanie szybkości gry
            self.stats.game_active = True
            self.stats.reset_stats()
            self.sb.prep_score()
            self.sb.prep_level()
            self.sb.prep_ships()

            #usunięcie zawartości list alien i bullet
            self.aliens.empty()
            self.bullets.empty()

            #utworzenie nowej floty i wyśrodkowanie statku
            self._create_fleet()
            self.ship.center_ship()

            #ukrycie kursora myszy po naciśnięciu przycisku
            pygame.mouse.set_visible(False)

    def _fire_bullet(self):
        """ Utworzenie nowego pocisku i dodanie go do grupy pocisków """
        if len(self.bullets) < self.settings.bullet_allowed:    # len() - poniewwarz potrzeba wartośći ilośći pocików
            new_bullet = Bullet(self) # utwozreniw pocisku
            self.bullets.add(new_bullet) # dodanie do grupu pocisków

    def _check_fleet_edges(self):
        """Reakcja na dodarcie obcego do krawędzi"""
        for alien in self.aliens.sprites():
            if alien.check_edges():
                self._chenge_fleet_direction()
                break

    def _check_aliens_bottom(self):
        """Sparwdza czy kturyś obcy dodatrł do dolnenj krawędzi """
        screen_rect = self.screen.get_rect()
        for alien in self.aliens.sprites():
            if alien.rect.bottom >= screen_rect.bottom:
                self._ship_hit()
                break

    def _check_bullet_alien_collision(self):
        """Reakcja na kolizję miedzy pociskiem a obcym"""
        #Sprawdzamy czy pocisk trafił obcego
        #Jeśli trafienie usuwamu pocisk i obcego
        collisions = pygame.sprite.groupcollide(self.bullets, self.aliens, True, True )
        if collisions:
            for aliens in collisions.values():
                self.stats.score += self.settings.alien_score * len(aliens)
            self.sb.prep_score()
            self.sb.prep_hight_score()

        if not self.aliens:  # sparwdzamy czy grupa obcych jest pusta, jak tak twozrymy nową flotę
            self.bullets.empty()  # pozbycie się isteniejących pocisków
            self._create_fleet()  # uwtorzenie nowej floty
            self.settings.increase_speed()

            # ziwękrzanie numeru poziomu
            self.stats.level += 1
            self.sb.prep_level()

    def _chenge_fleet_direction(self):
        """Zmiana kierunku i przejście floty obcych w dół"""
        for alien in self.aliens.sprites():
            alien.rect.y += self.settings.fleet_drop_speed
        self.settings.fleet_dierction *= -1

    def _create_fleet(self):
        """utworzenie floty obcych"""
        # utwozrenie żędu obcych i ustalenei odległości miedzy nimi
        # odległwść miedzy obcymi równa jest szerokości obcego
        alien = Alien(self)
        alien_width,  alien_height = alien.rect.size #ustawienie szerokowści i wyskości obcego w celu ustawinei odstępów
        available_space_x = self.settings.screen_width - ( 2 * alien_width)  # ustalene maksymalnje liczby obcych w rzedzie
        number_alien_x = available_space_x // (2 * alien_width)

        #ustalenie ile rzędów obcych zmieści się na ekranie
        ship_height = self.ship.rect.height #pobranie wyskowści statku
        available_space_y = (self.settings.screen_height - (3 * alien_height) - ship_height)
        number_rows = available_space_y // (2 * alien_height)

        #utworzenie pełnej floty obcych
        for row_number in range(number_rows):
            for alien_number in range(number_alien_x):
                self._create_alien(alien_number, row_number)

    def _create_alien(self, number_alien, row_number):
        """Utwozrenie obcego i umieszczenie go w rzędzie"""
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size #ustalenie sszerokości
        alien.x = alien_width + 2 * alien_width * number_alien
        alien.rect.x = alien.x
        alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
        self.aliens.add(alien)

    def _ship_hit(self):
        """ reakcja nazderzenei obcego ze statkiem """
        if self.stats.ship_left > 0:
            # zmniejszenie wartości w  zmienej ship_left
            self.stats.ship_left -= 1
            self.sb.prep_ships()
            # usunięcie zawartości list aliens i bullet
            self.aliens.empty()
            self.bullets.empty()

            # utwozrenie nowej floty i wyśridkowanie statku
            self._create_fleet()
            self.ship.center_ship()

            # Pauza
            time.sleep(1)
        else:
            self.stats.game_active = False
            pygame.mouse.set_visible(True)  # kursor myszy się pojawia po zakończeniu gry

    def _update_bullet(self):
        """ Uaktalnianie położenia pocisków """
        self.bullets.update()

        # usuwanie pocisków które znajdują sie poza ekranem
        for bullet in self.bullets.copy():
            if bullet.rect.bottom <= 0:  # jeśli pocisk dotzr do górnje krawędzi
                self.bullets.remove(bullet)  # usuwanie pocisku z grupy

        self._check_bullet_alien_collision()

    def _update_alien(self):
        """ Uaktualnienie położenia obcego """
        self._check_fleet_edges()
        self.aliens.update()

        # wykrycie koliji między obcym a statkiem
        if pygame.sprite.spritecollideany(self.ship, self.aliens):
            self._ship_hit()
        # wyszukiwanie obcych którzy dośli do dolnej krawędzi ekranu
        self._check_aliens_bottom()

    def _update_screen(self):
        """ Ukatualnaine obrazu na ekranie i przejście do nowego ekranu """
        # Oświeezanie ekranu w czasei wykonywania pętli
        self.screen.fill(self.settings.bg_color)
        self.ship.blitme()
        for bullet in self.bullets.sprites():  # wyświtlanie wrzystkich wystrzlonych pocisków
            bullet.draw_bullet()
        self.aliens.draw(self.screen)

        # Wyświetlanei informacji o punktacji
        self.sb.show_score()

        # wyświtlanie przycisku gdy gra jest nieaktywna
        if not self.stats.game_active:
            self.play_button.draw_button()

        # wyświtlanie ostatnio zmodyfikowanego ekranu
        pygame.display.flip()

if __name__ == '__main__':
    # utworzenie egzemplarza gry i jej uruchomienie
    al = AlienInvaders()
    al.run_game()

